
#include "motion-detector/motion_detector.h"

#define TAG	"core_app_main"

void core_app_main(void * param)
{
	CORE_LOGI(TAG, "==================================================================");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "Starting application built on the Iquester Core Embedded Framework");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "==================================================================");

	CORE_LOGI(TAG, "Calling motion_detector_init() in your_module.c in your-module directory ...");
	motion_detector_init();
	CORE_LOGI(TAG, "Returned from motion_detector_init()");

	while(1){
		//CORE_LOGI(TAG, "Calling your_module_loop() in your_module.c in your-module directory ...");
		motion_detector_loop();
		//CORE_LOGI(TAG, "Sleeping for %d ms before looping ...", DELAY_10_SEC);
		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}
