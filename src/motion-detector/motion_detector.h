/**
 * This is an example header file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#ifndef MOTION_DETECTOR_H_
#define MOTION_DETECTOR_H_

#include "emmate.h"
#include "thing.h"


typedef struct{
	bool motion_detect;
	uint64_t motion_detect_tm;
}MOTION_DETECTOR_PUBLISH_DATA;

/**
 *
 * */
void motion_detector_init();

/**
 * */
void motion_detector_loop();

#endif	/* MOTION_DETECTOR_H_ */
